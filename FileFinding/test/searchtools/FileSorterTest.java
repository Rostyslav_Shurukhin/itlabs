/*
 * FileSorterTest.java
 * JUnit based test
 *
 * Created on 21 �������� 2006 �., 17:26
 */

package searchtools;

import junit.framework.*;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.File;

/**
 *
 * @author �������� ��������
 * http://www.vova-prog.narod.ru
 */
public class FileSorterTest extends TestCase {
    
    public FileSorterTest(String testName) {
        super(testName);
    }

    public static Test suite() {
        TestSuite suite = new TestSuite(FileSorterTest.class);
        
        return suite;
    }

/*    public void testCompare() {
        System.out.println("compare");
        
        Object o1 = null;
        Object o2 = null;
        FileSorter instance = new FileSorter();
        
        int expResult = 0;
        int result = instance.compare(o1, o2);
        assertEquals(expResult, result);
        
        fail("The test case is a prototype.");
    }*/

    public void testSort() {
        System.out.println("sort");
        
        String startPath = "folderForTests";
        FileFinder finder = new FileFinder();
        List fileList = null;;
        try {
            fileList = finder.findAll(startPath);
            FileSorter sorter = new FileSorter();
            List res = sorter.sort(fileList);
            String fileName = ((File)res.get(0)).getName();
            assertTrue(fileName.equals("file1.txt"));
            fileName = ((File)res.get(1)).getName();
            assertTrue(fileName.equals("file2.txt"));
            fileName = ((File)res.get(2)).getName();
            assertTrue(fileName.equals("folder1"));
            fileName = ((File)res.get(3)).getName();
            assertTrue(fileName.equals("folder2"));
            fileName = ((File)res.get(4)).getName();
            assertTrue(fileName.equals("emptyFolder"));
            fileName = ((File)res.get(5)).getName();
            assertTrue(fileName.equals("file11.txt"));
            fileName = ((File)res.get(6)).getName();
            assertTrue(fileName.equals("file21.txt"));
            fileName = ((File)res.get(7)).getName();
            assertTrue(fileName.equals("file22.html"));
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }
    
}
