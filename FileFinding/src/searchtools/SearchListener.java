package searchtools;

public interface SearchListener {

    public void onSearchStart();

    public void onSearchProgressChange(long totalLength,
            long filesNumber, long directoriesNumber);
    
    public void onSearchEnd();
}
