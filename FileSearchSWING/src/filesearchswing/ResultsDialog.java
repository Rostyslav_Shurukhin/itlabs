/*
 * ResultsDialog.java
 *
 * Created on 18 ������� 2006 �., 20:20
 */

package filesearchswing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;
import javax.swing.*;

/**
 * ���� ����� ������������ ��� �������� ���� �� ������� �����������
 * ������
 *
 * @author �������� ��������
 * http://www.vova-prog.narod.ru
 */
public class ResultsDialog extends JDialog implements ActionListener {
    
    //������ �������� ����
    JButton closeButton = null;
    //��������� ������� � ������������
    JTextArea res = null;
    
    /** ������� ����� ���������� ResultsDialog */
    public ResultsDialog(JFrame owner, List searchResults) {
        //�������� ��������� ������������ ������������� ������.
        //������ �������� - ��������� �� ������� ���� ���������,
        //������ - ���������, ��� �� ������� ��������� ����
        super(owner, true);
        
        //������� ��������� �������
        res = new JTextArea(20, 80);
        //������� ���� ��������� (���������) � ��������� �
        //��� ��������� �������
        JScrollPane sp = new JScrollPane(res);
        //������������� ����������� ������ ���� ���������
        sp.setPreferredSize(new Dimension(400, 300));
        //�������� ���������� ������ � ��������� �������
        for(int i = 0; i < searchResults.size(); i++) {
            res.append(((File)searchResults.get(i)).getAbsolutePath()
                + "\n");
        }
        //������������� ������ � ������ ����
        res.setCaretPosition(0);
        //������������� �����
        res.setFont(new Font("Monospaced", Font.PLAIN, 14));
        //��������� ����
        setTitle("���������� ������");
        //��������� ���� ���������� � ��������� �������� ��
        //������ ����������� ����
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(sp, BorderLayout.CENTER);
        
        //������� � ��������� ����� ���� ������ "�������"
        closeButton = new JButton("�������");
        closeButton.setActionCommand("close");
        //����������� ���������� ������� ������
        closeButton.addActionListener(this);
        getContentPane().add(closeButton, BorderLayout.SOUTH);
    }

    /**
     * ���� ����� ���������� ��� ������� �� ������
     * @param e ������ � ����������� � ��������� ������� (��������,
     * � ���, ����� ������ ���� ������)
     */
    public void actionPerformed(ActionEvent e) {
        //���� ������ ������ "�������"
        if(e.getActionCommand().equals("close")) {
            //��������� ���������� ����
            setVisible(false);
        }
    }
}
