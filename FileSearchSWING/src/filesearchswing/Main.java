/*
 * Main.java
 *
 * Created on 16 ������� 2006 �., 20:02
 */

package filesearchswing;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.*;

/**
 *
 * @author �������� ��������
 * http://www.vova-prog.narod.ru
 */
public class Main implements WindowListener {
    
    static JFrame frame = null;
    static MainPanel contentPane = null;
    
    /** ������� ����� ���������� Main */
    public Main() {
        frame.addWindowListener(this);
    }
    
    private static void createAndShowGUI() {
        //������������� ������� ��� ����������
        try {
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        }
        catch(Exception err) {}
        
        //������� ������� �����
        frame = new JFrame("����� ������");
        //��������� ������ ��������� ������ ��� �������� ����
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        Main m = new Main();
        
        //������� ������, � ������ � ������� ������� ����
        contentPane = new MainPanel(frame);
        contentPane.setOpaque(true);
        frame.setContentPane(contentPane);
        
        //����������� ����� � ������ ��� �������
        frame.pack();
        frame.setVisible(true);
    }
    
    /**
     * ����� �����
     * @param args ��������� ��������� ������
     */
    public static void main(String[] args) {
        //������ ��������� � ��������� ����� ��� ������ � ���
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

    public void windowOpened(WindowEvent e) {
    }

    public void windowClosing(WindowEvent e) {
        contentPane.stopSearch();
        frame.dispose();
        System.exit(0);
    }

    public void windowClosed(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
    }

    public void windowActivated(WindowEvent e) {
    }

    public void windowDeactivated(WindowEvent e) {
    }
}
