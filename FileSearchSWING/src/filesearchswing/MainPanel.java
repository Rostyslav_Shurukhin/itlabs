/*
 * MainPanel.java
 *
 * Created on 17 ������� 2006 �., 16:20
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package filesearchswing;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;
import javax.swing.*;
import searchtools.*;

/**
 * ���� ����� ������������� ����� �������� ������ ���������,
 * �� ������� ����� ��������� �������� ����������.
 *
 * @author �������� ��������
 * http://www.vova-prog.narod.ru
 */
public class MainPanel extends JPanel implements SearchListener,
        ActionListener, Runnable {
    
    //������� ��������� �� �������� ����������
    private JTextField folderName = null;
    private JTextField patternField = null;
    private JLabel patternLabel = null;
    private JLabel filesNumberLabel = null;
    private JLabel foldersNumberLabel = null;
    private JLabel filesSizeLabel = null;
    private JButton selectFolder = null;
    private JButton startSearch = null;
    private JFileChooser fc = null;
    
    //������� ��������� ��������� (����� � JLabel)
    private final String FILES_NUMBER_LABEL = "������� ������: ";
    private final String FOLDERS_NUMBER_LABEL = "������� �����: ";
    private final String FILES_SIZE_LABEL =
            "������ ��������� ������ (����): ";
    
    //��������� ����� ������
    private File startFolder = null;
    //��� ���������
    private FileFinder finder = null;
    //�����, � ������� ����� ����������� �����
    private Thread searchThread = null;
    //��������� �� ������� ����� ��������� (����� ��� ��������
    //����������� ����)
    private JFrame frame = null;
    
    /**
     * ������� ����� ���������� MainPanel
     * @param frame ��������� �� ������� ����� ���������
     */
    public MainPanel(JFrame frame) {
        //��� ���������� �� ������ ����� ������������� ����
        //��� ������
        setLayout(new GridLayout(0,1));
        
        this.frame = frame;
        
        //������� ����������
        //��������� ���� (��� �������� ��������� �����)
        folderName = new JTextField(40);
        //������� � ��������� ���� ��� ����� ����������� ���������
        patternLabel = new JLabel("���������� ���������");
        patternField = new JTextField(40);
        //JLabel, � ������� ����� ������������ ��������� ������
        filesNumberLabel = new JLabel(FILES_NUMBER_LABEL);
        foldersNumberLabel = new JLabel(FOLDERS_NUMBER_LABEL);
        filesSizeLabel = new JLabel(FILES_SIZE_LABEL);
        //������ "�����..."
        selectFolder = new JButton("�����...");
        //������ "�����"
        startSearch = new JButton("�����");
        
        //����������� ���������� ������� ������� �� ������
        selectFolder.addActionListener(this);
        selectFolder.setActionCommand("browse");
        startSearch.addActionListener(this);
        startSearch.setActionCommand("search");
        
        //��������� ���������� �� ������
        add(folderName);
        add(selectFolder);
        add(patternLabel);
        add(patternField);
        add(filesNumberLabel);
        add(foldersNumberLabel);
        add(filesSizeLabel);
        add(startSearch);
        
        //������� ���������� ���� ��� ������ ��������� �����
        fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        
        //������� ���������
        finder = new FileFinder();
        //������������ �����, ������� ����� �������� �����������
        //� �������� ������
        finder.addListener(this);
    }

    /**
     * ���� ����� ���������� ����������� ������ FileFinder
     * � ������ ������
     */
    public void onSearchStart() {
        //��������� ��������� ���������
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                filesNumberLabel.setText(FILES_NUMBER_LABEL + "0");
                foldersNumberLabel.setText(FOLDERS_NUMBER_LABEL + "0");
                filesSizeLabel.setText(FILES_SIZE_LABEL + "0");
            }
        });
    }

    /**
     * ���� ����� ���������� ����������� ������ FileFinder
     * ������������ � �������� ������
     * @param totalLength ����� ������ ��������� ������
     * @param filesNumber ���������� ��������� ������
     * @param directoriesNumber ���������� ��������� �����
     */
    public void onSearchProgressChange(long totalLength,
            long filesNumber, long directoriesNumber) {
        //�.�. ���������� ���������� ��������� ����������� �
        //��������� ������, �� �� ����� �������� ������������
        //��������� ������. ��� ����, ����� ������ ��� �����������
        //������� ���������, � ����������� �� �������� ����������.
        final long fn = filesNumber;
        final long dn = directoriesNumber;
        final long fl = totalLength;
        //��������� ���������
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                filesNumberLabel.setText(FILES_NUMBER_LABEL + fn);
                foldersNumberLabel.setText(FOLDERS_NUMBER_LABEL + dn);
                filesSizeLabel.setText(FILES_SIZE_LABEL + fl);
            }
        });
    }

    /**
     * ���� ����� ���������� ����������� ������ FileFinder
     * ���� ��� ��� ���������� ������
     */
    public void onSearchEnd() {
        //�.�. ���������� ���������� ��������� ����������� �
        //��������� ������, �� �� ����� �������� ������������
        //��������� ������. ��� ����, ����� ������ ��� �����������
        //������� ���������, � ����������� �� �������� ����������.
        final long fn = finder.getFilesNumber();
        final long dn = finder.getDirectoriesNumber();
        final long fl = finder.getDirectorySize();
        //��������� ���������
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                filesNumberLabel.setText(FILES_NUMBER_LABEL + fn);
                foldersNumberLabel.setText(FOLDERS_NUMBER_LABEL + dn);
                filesSizeLabel.setText(FILES_SIZE_LABEL + fl);
                //������������� ������� ������ "�����" (� ��������
                //������ ����������� "����")
                startSearch.setText("�����");
                startSearch.setActionCommand("search");
            }
        });
    }

    /**
     * ���� ����� ���������� ��� ������� �� ������ ���������
     * @param e ������ � ����������� � ��������� ������� (��������,
     * � ���, ����� ������ ���� ������)
     */
    public void actionPerformed(ActionEvent e) {
        //��������� ������� ������ "�����..."
        if(e.getActionCommand().equals("browse")) {
            //��������� ������ ������ �����
            int retVal = fc.showOpenDialog(this);
            //���� ����� �������...
            if(retVal == JFileChooser.APPROVE_OPTION) {
                //...������������� � � �������� ��������� �����
                startFolder = fc.getSelectedFile();
                //� ���������� ������ ���� � ��������� ����
                folderName.setText(startFolder.getAbsolutePath());
            }
        }
        //��������� ������� ������ "�����"
        if(e.getActionCommand().equals("search")) {
            //������� � ��������� ����� �����, � ������� �����
            //����������� �����
            searchThread = new Thread(this);
            searchThread.start();
            //������������� ��� ������ ������� "����"
            startSearch.setText("����������");
            startSearch.setActionCommand("stop");
        }
        //��������� ������� ������ "����"
        if(e.getActionCommand().equals("stop")) {
            stopSearch();
            //������������� ��� ������ ������� "�����"
            startSearch.setText("�����");
            startSearch.setActionCommand("search");
        }
    }

    public void stopSearch() {
        //������������� �����
        finder.stopSearch();
        //���� ���������� ������, � ������� ����������� �����
        if(searchThread != null) {
            while(searchThread.isAlive()) {
                Thread.currentThread().yield();
            }
        }
    }

    /**
     * ���� ����� ����������� � ��������� ������
     * (� ��� � ����������� �����)
     */
    public void run() {
        try {
            //������ ���������� ���������
            String mask = patternField.getText();
            //�������� �����
            final List res = finder.findAll(folderName.getText(), mask);
            //����� ���������� ������ ������� ���������� ���� ��
            //������� ��������� ��������
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    ResultsDialog rd = new ResultsDialog(frame, res);
                    rd.pack();
                    rd.setVisible(true);
                }
            });
        }
        catch(Exception err) {
            //������ ���� � ����������� �� �������, ����
            //��� ��������
            final String error = err.getMessage();
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    JOptionPane.showMessageDialog(frame, error,
                            "������", JOptionPane.ERROR_MESSAGE);
                }
            });
        }
    }
}
